## archiveteam-infra

Ready to save the web, one archiveteam project at a time? Then this project is for you!

## Installation

Get a DigitalOcean API Token. https://cloud.digitalocean.com/account/api/tokens/new

Upload your ssh key to DigitalOcean. You can do that here https://cloud.digitalocean.com/account/security?i=c84be4 Make a copy of the ssh fingerprint as you need it later.

Copy `terraform.tfvars.dist` to `terraform.tfvars`, filling out the wanted values.

Now run `terraform apply`. Once it's done, you're helping! Congratz.

## TODO

> What is missing, what would make this project better?

- [ ] Integrated dashboard with Grafana
- [ ] Federated dashboard with prometheus, integrating many deployments into one dashboard

### LICENSE: MIT (2018) DIGGAN
